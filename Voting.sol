// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {
    address public chairperson;

    mapping(address => Voter) public voters;

    uint numberOfVoters = 0;
    uint numberOfParticipants = 0;

    Proposal[] public proposals;
    uint winningProposalId;

    WorkflowStatus step;

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted (address voter, uint proposalId);

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    struct Voter {
        uint voteIndex;
        bool hasVoted;
        bool isRegistered;
        uint weight;
    }

    struct Proposal {
        string name;
        uint voteCount;
    }

    constructor(uint chairpersonWeight) Ownable(msg.sender) {
        chairperson = msg.sender;
        voters[chairperson].isRegistered = true;
        voters[chairperson].weight = chairpersonWeight;
        step = WorkflowStatus.RegisteringVoters;
        numberOfParticipants++;
    }

    function getNumberOfVotes() public view onlyOwner returns (uint numberOfVotes_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        numberOfVotes_ = numberOfVoters;
    }

    function getProposals() public view returns (string[] memory titles) {
        titles = new string[](proposals.length);
        for (uint256 i = 0; i < proposals.length; i++) {
            titles[i] = proposals[i].name;
        }
    }

    function getAbstentionPercentage() public view returns (uint abstentionPercentage_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        abstentionPercentage_ = (numberOfParticipants - numberOfVoters) * 100 / numberOfParticipants;
    }

    function getNumberOfParticipants() public view returns (uint numberOfParticipants_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        numberOfParticipants_ = numberOfParticipants;
    }

    function getWinner() external view returns (string memory winnerName_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        winnerName_ = proposals[winningProposal()].name;
    }

    function changeStep(uint newStep) external onlyOwner {
        require(uint(WorkflowStatus.RegisteringVoters) <= newStep && newStep <= uint(WorkflowStatus.VotesTallied), "Unknown step.");
        WorkflowStatus previousStep = step;
        step = WorkflowStatus(newStep);
        emit WorkflowStatusChange(previousStep, step);
    }

    function giveRightToVote(address voter, uint weight) external onlyOwner {
        require(step == WorkflowStatus.RegisteringVoters, "Registering voters step is not started.");
        require(!voters[voter].hasVoted, "The voter already voted.");
        require(voters[voter].isRegistered == false, "Already added.");
        voters[voter].isRegistered = true;
        voters[voter].weight = weight;
        numberOfParticipants++;
        emit VoterRegistered(voter);
    }

    function vote(uint proposal) external {
        require(step == WorkflowStatus.VotingSessionStarted, "Voting time is not started.");
        Voter storage sender = voters[msg.sender];
        require(sender.isRegistered, "Has no right to vote.");
        require(!sender.hasVoted, "Already voted.");
        sender.hasVoted = true;
        sender.voteIndex = proposal;
        numberOfVoters++;
        proposals[proposal].voteCount += sender.weight;
        emit Voted(msg.sender, proposal);
    }

    function registerProposal(string memory proposalName) external {
        require(step == WorkflowStatus.ProposalsRegistrationStarted, "Proposals registration is not started.");
        Voter storage sender = voters[msg.sender];
        require(sender.isRegistered, "Has no right to register proposal.");
        proposals.push(Proposal({name: proposalName, voteCount: 0}));
        emit ProposalRegistered(proposals.length - 1);
    }

    function tallyVotes() public onlyOwner {
        require(step == WorkflowStatus.VotingSessionEnded, "Not time to count votes.");
        step = WorkflowStatus.VotesTallied;
        uint winningVoteCount = 0;
        for (uint p = 0; p < proposals.length; p++) {
            if (proposals[p].voteCount > winningVoteCount) {
                winningVoteCount = proposals[p].voteCount;
                winningProposalId = p;
            }
        }
    }

    function winningProposal() public view returns (uint winningProposal_) {
        require(step == WorkflowStatus.VotesTallied, "Not time to check results.");
        winningProposal_ = winningProposalId;
    }
}
